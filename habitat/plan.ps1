$pkg_name="my_package"
$pkg_origin="make_an_origin"
$pkg_version="0.0.1"
$pkg_maintainer="robertjskelton@gmail.com"
$pkg_description="$scaffold_policy_name"
$pkg_upstream_url="http://chef.io"
$pkg_scaffolding="chef/scaffolding-chef-infra"
$scaffold_policy_name="to_add"
$scaffold_chef_client = "chef/chef-infra-client"
